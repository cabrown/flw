import curses                                                                                                                         

def main(stdscr):                                                       
    if curses.has_colors():                                             
        curses.start_color()                                            
        curses.use_default_colors()                                     
        for i in range(0, curses.COLORS):                               
            #curses.init_pair(i+1, 253, i)     # opt2                    
            curses.init_pair(i+1, i, 253)      # opt1                  
    try:                                                                
        #stdscr.bkgd(' ', curses.color_pair(8) | curses.A_REVERSE)  # opt2
        stdscr.bkgd(' ', curses.color_pair(8))                      # opt1
        for i in range(0, curses.COLORS):                               
            stdscr.addstr(str(i), curses.color_pair(i))                 

    except:                                                  
        print('Exception caught')                                         
    stdscr.refresh()                                                    
    stdscr.getch()                                                      

curses.wrapper(main) 