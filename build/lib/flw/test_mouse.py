import curses 

screen = curses.initscr() 
#curses.noecho() 
curses.curs_set(0) 
screen.keypad(1) 
curses.mousemask(1)

i = 0
while True:
    event = screen.getch() 
    if event == ord("q"): break 
    if event == curses.KEY_MOUSE:
        _, mx, my, _, _ = curses.getmouse()
        screen.addstr(0, 0, f"Mouse click: {mx}, {my}") #screen.instr(my, mx, 5))
        screen.clrtoeol()
        screen.refresh()

curses.endwin()