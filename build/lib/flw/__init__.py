import sys,os
import curses

# TODO: [DONE] Implement callbacks and keybindings
# TODO: [IN PROGRESS] Create group widget, which can be a parent to other widgets, has its own canvas, etc
#       group should inherit from widget. Widget_manager should inherit from group & add main loop etc
# TODO: [DONE] make flat border option
# TODO: [DONE] Implement colors
# TODO: Allow storage of widgets/params/layout in dict / yaml file
# TODO: [DONE] Reorganize stored widgets to make them easier to access in callbacks. 
#       EG, wm.widget_focus is always the widget with focus. And don't store
#       them in lists, store in a class accessible by name: wm.widgets.list1.value
# TODO: [DONE] Implement wm.widget_focus, which always holds the widget that has focus, 
# TODO: [DONE] Implement focus_prev and focus_next in wm
# TODO: [DONE] Prepend all internal params with _ per convention
# TODO: Add initial_ind param to listbox. None means don't select at start
# TODO: Add fmt_select to listbox; fmt_mouse_down
# TODO: widget.visible bool param
# TODO: widget.enabled bool param
# TODO: Disabled widget fmt
# TODO: Use color class instead of params
# TODO: [DONE] Button widget, inherit from label
# TODO: [DONE] Fix button click callback
# TODO: Slider/Progress widget
# TODO: Textbox widget
# TODO: Pulldown widget
# TODO: Jesus. Label still crashes on resize off screen
# TODO: Redo checkbox; inherit from label
# TODO: Custom checkbox checks, including multi-character (eg, 2x2 square etc)
# TODO: Auto layout from python objects like guidata: https://github.com/PierreRaybaut/guidata
# TODO: [IN PROGRESS] Dialogs: messagebox, yes/no, file picker, etc
# TODO: [DONE] messagebox mouse clicks
# TODO: Scroll main window and group widget for endless forms
# TODO: Custom border characters
# TODO: Implement z-order
# TODO: Remove curses pad usage, implement scrolling by hand in prep for scrolling as a widget feature 
# TODO: Proper widget right-justify and center
# TODO: Check if group widget w,h is dependent on children; if so, compute their dims first. 
#       Otherwise compute self first
# TODO: [DONE] Fix focus
# TODO: [DONE: render twice ain't great but it works] Fix initial render
# TODO: Inherit fmt from parent
# TODO: [DONE] Get rid of passing parent dims to widget render
# TODO: [DONT FIX: probably should be kept separate] Merge update_contents and resize_event. 
#       if self._orig_w == 0: #(0 means scale to fit contents)
#           compute children dims
#           resize based on that
#       else:
#           if 0 < self._orig_w < 1: #(float means proportional to parent)
#               compute parent dims
#               resize based on that
#           else:
#               width is fixed val
# TODO: Group widget does not expand to fit widgets, only stretches to fill available space, or is 
#       a fixed size


class color():
    # 3-element tuples: (fg,bg,style)
    # where style can be curses.A_BOLD, etc
    main =  (-1, -1, curses.A_NORMAL)
    border = (-1, -1, curses.A_NORMAL)


class focus():
    focus = color()
    nofocus = color()


class color_group():
    enabled = focus()
    disabled = color()


class widget():
    """ Superclass widget; useless on its own
    """
    class colors(): # Implement: This isn't used ATM
        # self.enabled.focus.border
        # self.enabled.nofocus.border
        # self.disabled.border
        pass


    def __init__(self, root, parent, name, **kwargs):

        self.event_types = ['on_initialize', 'on_resize', 'on_got_focus', 'on_lost_focus', 'on_change', 'on_keypress', 'on_click', 'on_exit']
        self._root = root
        self._parent = parent
        self._win = root._win
        self.name = name
        self._got_dims = False
        self._x_orig = kwargs.get('x', 0)
        self._y_orig = kwargs.get('y', 0)
        self._w_orig = kwargs.get('w', 0)
        self._h_orig = kwargs.get('h', 0)
        self._prev_focus = False
        if not hasattr(self, 'disabled'): self.disabled = kwargs.get('disabled', False)
        if not hasattr(self, 'border'): self.border = kwargs.get('border', 'flat')
        if not hasattr(self, 'get_focus'): self.get_focus = kwargs.get('get_focus', False)
        if not hasattr(self, 'key_bindings'): self._key_bindings = kwargs.get('key_bindings', {})
        if not hasattr(self, 'event_bindings'): self._event_bindings = kwargs.get('event_bindings', {})
        if not hasattr(self, 'value'): self.value = kwargs.get('value', None)

        # Process formatting stuff
        self.sysdefault_fg,self.sysdefault_bg = curses.pair_content(0)
        if not hasattr(self, 'fmt_main_norm'): self._fmt_main_norm = kwargs.get('fmt_main_norm', (-1,-1, curses.A_NORMAL))
        if not hasattr(self, 'fmt_main_focus'): self._fmt_main_focus = kwargs.get('fmt_main_focus', (-1,-1, curses.A_NORMAL))
        if not hasattr(self, 'fmt_border_norm'): self._fmt_border_norm = kwargs.get('fmt_border_norm', (-1,-1, curses.A_NORMAL))
        if not hasattr(self, 'fmt_border_focus'): self._fmt_border_focus = kwargs.get('fmt_border_focus', (-1,-1, curses.A_BOLD))

        self._fmt_main = {}
        self._fmt_border = {}
        self._fmt_main[False] = curses.color_pair(self._root.get_color_pair_ind(self._fmt_main_norm[:2])) #| self._c_main_norm[2]
        self._fmt_main[True] = curses.color_pair(self._root.get_color_pair_ind(self._fmt_main_focus[:2])) #| self._c_main_focus[2]
        self._fmt_border[False] = curses.color_pair(self._root.get_color_pair_ind(self._fmt_border_norm[:2])) #| self._c_border_norm[2]
        self._fmt_border[True] = curses.color_pair(self._root.get_color_pair_ind(self._fmt_border_focus[:2])) #| self._c_border_focus[2]

        self.update_contents()
        self.resize_event()
        self.do_event('on_initialize')
        

    def update_contents(self):
        # To be called whenever the contents of a widget change, so that x,y,w,h can be re-computed correctly
        pass


    def draw_border(self, win, uly, ulx, lry, lrx, focus=False):
        """ Draws a border around a widget

            a flat border requires a colorpair in which the bg 
            color is the widget bg color, and the fg is the parent 
            fg color

            a line border is the more traditional box-drawing border
        """
        if self.border=='flat':
            for i in range(uly + 1,lry):
                win.addstr(i, ulx, "\u2590", self._fmt_border[focus])
                win.addstr(i, lrx, "\u258C", self._fmt_border[focus])
            for i in range(ulx + 1, lrx):
                win.addstr(uly, i, "\u2584", self._fmt_border[focus])
                win.addstr(lry, i, "\u2580", self._fmt_border[focus])
            win.addstr(uly, ulx, "\u2597", self._fmt_border[focus])
            win.addstr(uly, lrx, "\u2596", self._fmt_border[focus])
            #win.addstr(lry, lrx, "\u2598", self._fmt_border[focus])
            #win.addstr(lry, ulx, "\u259D", self._fmt_border[focus])
        elif self.border=='line':
            win.bkgd(' ', curses.color_pair(1))
            win.vline(uly + 1, ulx, curses.ACS_VLINE, lry - uly - 1, self._fmt_border[focus])
            win.hline(uly, ulx + 1, curses.ACS_HLINE, lrx - ulx - 1, self._fmt_border[focus])
            win.hline(lry, ulx + 1, curses.ACS_HLINE, lrx - ulx - 1, self._fmt_border[focus])
            win.vline(uly + 1, lrx, curses.ACS_VLINE, lry - uly - 1, self._fmt_border[focus])
            win.addch(uly, ulx, curses.ACS_ULCORNER, self._fmt_border[focus])
            win.addch(uly, lrx, curses.ACS_URCORNER, self._fmt_border[focus])
            win.addch(lry, lrx, curses.ACS_LRCORNER, self._fmt_border[focus])
            win.addch(lry, ulx, curses.ACS_LLCORNER, self._fmt_border[focus])


    def do_event(self, event):
        for cb,k in self._event_bindings.items():
            if isinstance(k, list) and event in k or event == k:
                cb(self)
                break


    def resize_event(self):
        """ Compute x,y,width,height of widget given requested parameters and screen dims
        """
        screen_h, screen_w = self._win.getmaxyx()
        if self._got_dims: 
            prev_x = self.x
            prev_y = self.y
            prev_w = self.w
            prev_h = self.h

        if self._w_orig == 0:
            # Set width to be only as wide as needed
            self.w = self.contents_w
            if self.border:
                self.w += 2
        elif self._w_orig < 1 and self._w_orig > 0:
            # Set width to be proportion of screen width
            self.w = int(self._w_orig * screen_w)
        elif self._w_orig >= 1:
            # Set width to be exactly as specified
            self.w = int(self._w_orig)
        else:
            raise Exception("Widget width must be int==0, 0>float>=1, or int>1")

        if self._h_orig == 0:
            # Set height to be only as high as needed
            self.h = self.contents_h
            if self.border:
                self.h += 2
        elif self._h_orig > 0 and self._h_orig < 1:
            # Set height to be proportional to screen height
            self.h = int(self._h_orig * screen_h)
        elif self._h_orig >= 1:
            # Set height to be exactly as specified
            self.h = int(self._h_orig)
        else:
            raise Exception("Widget width must be int==0, 0>float>=1, or int>1") 

        if self._x_orig == None:
            # Center widget on screen
            self.x = screen_w // 2 - self.w // 2
        elif self._x_orig < 0:
            # Right justify widget on screen
            self.x = screen_w - self.w + self._x_orig # x_orig is negative, so add
        elif self._x_orig > 0 and self._x_orig < 1:
            # Position widget proportionally to screen width
            self.x = int(self._x_orig * screen_w)
        else:
            # Position widget as specified
            self.x = int(self._x_orig)
        self.x_abs = self.x + self._parent.x

        if self._y_orig == None:
            # Center widget on screen
            self.y = screen_h // 2 - self.h // 2
        elif self._y_orig < 0:
            # Bottom justify widget on screen
            self.y = screen_h - self.h + self._y_orig
        elif self._y_orig > 0 and self._y_orig < 1:
            # Position widget proportionally to screen height
            self.y = int(self._y_orig * screen_h)
        else:
            # Position widget as specified
            self.y = int(self._y_orig)
        self.y_abs = self.y + self._parent.y

        if self._got_dims:
            if self.x != prev_x or self.y != prev_y or self.w != prev_w or self.h != prev_h:
                self.do_event('on_resize')
        else:
            self._got_dims = True


    def render(self):
        """ Draws widget. Since each widget will draw itself differently
            this doesn't actually draw anything. It only keeps track of
            focus events, so should be called by all subclasses.
        """
        if self._root.widget_focus==self and not self._prev_focus:
            self.do_event('on_got_focus')
        elif not self._root.widget_focus==self and self._prev_focus:
            self.do_event('on_lost_focus')
        self._prev_focus = self._root.widget_focus==self


    def key_event(self, key):
        """ Check for key in key_bindings; process associated callback if found
        """
        for k,cb in self._key_bindings.items():
            # k can be a list or a number
            if isinstance(k, list) and key in k or key == k:
                # Found key; send self to the specified function for user processing
                self.current_key = key
                cb(self)
                self.current_key = None
                break
        self.do_event('on_keypress')


    def mouse_event(self, mid, mx, my, mz, bstate):
        """ No default mouse behavior. See listbox for an example of how to process mouse events
        """
        self.do_event('on_click')


    def exit_event(self):
        self.do_event('on_exit')


class label(widget):
    """ A label

        Parameters
        ----------
        x=0
        y=0
        w=0
        h=0
        disabled=False
        border=False
        get_focus=False
        key_bindings={}
        event_bindings={}
        justify='left'
    """
    def __init__(self, root, parent, name, **kwargs):
        if 'border' in kwargs and kwargs['border'] in root.border_types:
            self.pad = 2
            self.off = 1
        else:
            self.pad = 0
            self.off = 0
        if not hasattr(self, 'justify'): self.justify = kwargs.get('justify', 'left')
        super().__init__(root=root, parent=parent, name=name, **kwargs)


    def update_contents(self):
        self.contents_h = self.value.count("\n") + 1
        self.contents_w = 0
        for line in self.value.split("\n"):
            self.contents_w = max(self.contents_w, len(line))
        self.do_event('on_update')


    def render(self):
        screen_h,screen_w = self._win.getmaxyx()
        if self.x < screen_w and self.y < screen_h:
            #if self.x < screen_w and self.x + self.w >= screen_w - 3: # Don't understand why 3 works, but...
            if self.x + self.w >= screen_w:
                view_w = screen_w - self.x - 1
            else:
                view_w = self.w
            if self.y < screen_h and self.y + self.h >= screen_h - 1:
                view_h = screen_h - self.y
            else:
                view_h = self.h

            cpad = curses.newpad(self.contents_h + self.pad, self.contents_w + self.pad + 2 + 1)
            cpad.bkgd(' ', self._fmt_main[self._root.widget_focus==self])
            # Draw
            for idx, row in enumerate(self.value.split('\n')):
                if self.justify == 'left':
                    cpad.addstr(idx + self.off, self.off, row.ljust(self.contents_w - 1), self._fmt_main[self._root.widget_focus==self])
                elif self.justify == 'right':
                    cpad.addstr(idx + self.off, self.off, row.rjust(self.contents_w - 1), self._fmt_main[self._root.widget_focus==self])
                else:
                    cpad.addstr(idx + self.off, self.off, row.center(self.contents_w - 1), self._fmt_main[self._root.widget_focus==self])

            self.draw_border(cpad, 0, 0, self.contents_h+self.pad - 1, self.contents_w + self.pad, self._root.widget_focus==self)

            # buffery, bufferx, screeny1, screenx1, screeny2, screenx2
            #cpad.refresh(0, 0, self.y, self.x, self.y + view_h + 1, self.x + view_w)
            cpad.refresh(0, 0, self._parent.y + self.y, self._parent.x + self.x, self.y + view_h, self.x + view_w)

        if self._root.widget_focus==self and self.get_focus:
            super().render()


    def mouse_event(self, mid, mx, my, mz, bstate):
        # Some Linux modes only report clicks, so check for any
        # button down or click events.
        if (bstate & curses.BUTTON1_PRESSED != 0 or bstate & curses.BUTTON1_CLICKED != 0):
            if my >= self.y + self.off and my < self.y + self.h - self.off and mx > self.x + self.off and mx < self.x + self.w - self.off - 1:
                self.do_event('on_click')


    def key_event(self, key):
        # Send key to super for keybinding processing
        super().key_event(key)
        self.do_event('on_keypress')


class button(label):
    def __init__(self, root, name, value, callback, **kwargs):
        self.callback = callback
        self.get_focus = kwargs.get('get_focus', True)
        super().__init__(root=root, name=name, value=value, **kwargs)


    def mouse_event(self, mid, mx, my, mz, bstate):
        # The whole widget is the button, and we already know it has been clicked
        # from the wm hittest, so no need to check anything here, just do callback
        self.callback(self._root)
        self.do_event('on_click')


    def key_event(self, key):
        if key in self._root.keys_enter:
            #self.change_event()
            self.callback(self._root)
            #self.do_event('on_click')
        else:
            # Send key to super for keybinding processing
            super().key_event(key)
        self.do_event('on_keypress')


class checkbox(widget):
    def __init__(self, root, parent, name, value, text, **kwargs):
        self.char_unchecked = '\u2610'
        self.char_checked = '\u2611'
        self.text = text
        if 'border' in kwargs and kwargs['border'] in root.border_types:
            self.pad = 2
            self.off = 1
        else:
            self.pad = 0
            self.off = 0
        self.get_focus = kwargs.get('get_focus', True)
        self.justify = kwargs.get('justify', 'left')
        super().__init__(root=root, parent=parent, name=name, value=value, **kwargs)
        self.event_types.append('on_check')
        self.event_types.append('on_uncheck')


    def update_contents(self):
        self.contents_h = self.text.count("\n") + 1
        self.contents_w = 0
        for line in self.text.split("\n"):
            self.contents_w = max(self.contents_w, len(line))
        self.contents_w += 2 # 2 for space-checkbox
        self.do_event('on_update')


    def render(self):
        screen_h,screen_w = self._win.getmaxyx()
        if self.x < screen_w and self.y < screen_h:
            if self.x < screen_w and self.x + self.w >= screen_w - 3: # Don't understand why 3 works, but...
                view_w = screen_w - self.x
            else:
                view_w = self.w
            if self.y < screen_h and self.y + self.h >= screen_h - 1:
                view_h = screen_h - self.y
            else:
                view_h = self.h

            cpad = curses.newpad(self.contents_h + self.pad + 1, self.contents_w + self.pad + 2)
            cpad.bkgd(' ', self._fmt_main[self._root.widget_focus==self])
            icon_y =  self.contents_h // 2
            if self.value:
                icon = self.char_checked
            else:
                icon = self.char_unchecked

            # Draw
            for idx, row in enumerate(self.text.split('\n')):
                if self.justify == 'left':
                    cpad.addstr(idx + self.off, self.off + 2, row.ljust(self.contents_w - 3)) #, attr)
                    if idx == icon_y:
                        cpad.addstr(idx + self.off, self.off, icon,curses.A_BOLD)
                elif self.justify == 'right':
                    cpad.addstr(idx + self.off, self.off, row.rjust(self.contents_w - 3)) #, attr)
                    if idx == icon_y:
                        cpad.addstr(idx + self.off, self.contents_w, icon,curses.A_BOLD)
                else:
                    cpad.addstr(idx + self.off, self.off + 2, row.center(self.contents_w - 3)) #, attr)
                    if idx == icon_y:
                        cpad.addstr(idx + self.off, self.off, icon,curses.A_BOLD)

            self.draw_border(cpad, 0, 0, self.contents_h+self.pad - 1, self.contents_w + self.pad, focus=self._root.widget_focus==self)

            # TODO: Update this to add parent x & y
            # buffery, bufferx, screeny1, screenx1, screeny2, screenx2
            cpad.refresh(0, 0, self._parent.y + self.y, self._parent.x + self.x, self.y + view_h + 1, self.x + view_w)

        if self._root.widget_focus==self and self.get_focus:
            super().render()


    def mouse_event(self, mid, mx, my, mz, bstate):
        # Some Linux modes only report clicks, so check for any
        # button down or click events.
        if (bstate & curses.BUTTON1_PRESSED != 0 or bstate & curses.BUTTON1_CLICKED != 0):
            if my >= self.y + self.off and my < self.y + self.h - self.off and mx > self.x + self.off and mx < self.x + self.w - self.off - 1:
                self.change_event()
                self.do_event('on_click')


    def change_event(self):
        if self.value:
            self.value = False
            self.do_event('on_uncheck')
        else:
            self.value = True
            self.do_event('on_check')
        self.do_event('on_change')


    def key_event(self, key):
        if key == ord(' '):
            self.change_event()
        else:
            # Send key to super for keybinding processing
            super().key_event(key)
        self.do_event('on_keypress')


class listbox(widget):
    def __init__(self, root, parent, name, options, **kwargs):
        self.options = options        # List of options to display
        self.row_idx_curr = 0   # which idx to highlight
        self.row_idx_view = 0   # which idx to display as first line
        if 'border' in kwargs and kwargs['border'] in root.border_types:
            self.pad = 2
            self.off = 1
        else:
            self.pad = 0
            self.off = 0
        self.value = options[0]
        self.justify = kwargs.get('justify', 'left')
        self.get_focus = kwargs.get('get_focus', True)
        super().__init__(root=root, parent=parent, name=name, **kwargs)


    def update_contents(self):
        self.contents_h = len(self.options)
        self.contents_w = 0
        for item in self.options: # Find the longest item in options
            self.contents_w = max(self.contents_w, len(item))
        self.do_event('on_update')


    def render(self):
        # TODO: Deal with parent_w and parent_h
        """ Draw listbox

            Always create entire list, and then only show the part needed,  
            overwriting the border in the correct location as needed.
        """
        # Create buffer; always full height based on options and self.w
        cpad = curses.newpad(int(max(self.contents_h + self.pad, self.h)), self.w + 1) # Add 1 to width to avoid crash on drawing lr corner in border
        #cpad.bkgd(' ', curses.color_pair(self._fmt_main[focus]))

        # Populate
        for idx, row in enumerate(self.options):
            if idx == self.row_idx_curr:
                attr = self._fmt_main[self._root.widget_focus==self] | curses.A_REVERSE
            else:
                attr = self._fmt_main[self._root.widget_focus==self]
            if self.justify == 'left':
                cpad.addstr(int(idx + self.off), 1, " " + row.ljust(int(self.w - 3)), attr)
            elif self.justify == 'right':
                cpad.addstr(idx + self.off, 1, row.rjust(self.w - 3) + " ", attr)
            else:
                cpad.addstr(idx + self.off, 1, row.center(self.w - 2), attr)

        self.draw_border(cpad, self.row_idx_view, 0, self.row_idx_view + self.h - 1, self.w - 1, focus=self._root.widget_focus==self)

        if self.row_idx_view > 0:
            cpad.addch(self.row_idx_view, self.w - 1, '\u02C4') #\u25B2') # curses.ACS_UARROW) # '\u25B2'
        if self.row_idx_view + self.h < len(self.options) + self.pad:
            cpad.addch(self.row_idx_view + self.h - 1, self.w - 1, '\u02C5') #\u2B07') #\u25BC') # curses.ACS_DARROW) # '\u25BC'

        # Ensure only visible part is drawn to avoid curses exception
        screen_h,screen_w = self._win.getmaxyx()
        if self.x + self.w >= screen_w - 1:
            view_w = screen_w - 1
        else:
            view_w = self.x + self.w -1
        if self.y + self.h - 1 >= screen_h - 1:
            view_h = screen_h - 1
        else:
            view_h = self.y + self.h - 1

        if self.x < screen_w and self.y < screen_h:
            # buffery, bufferx, screeny1, screenx1, screeny2, screenx2
            cpad.refresh(self.row_idx_view, 0, self._parent.y + self.y, self._parent.x + self.x, self._parent.y + view_h, self._parent.x + view_w)

        if self._root.widget_focus==self and self.get_focus:
            super().render()


    def mouse_event(self, mid, mx, my, mz, bstate):
        # Some Linux modes only report clicks, so check for any
        # button down or click events.
        if (bstate & curses.BUTTON1_PRESSED != 0 or bstate & curses.BUTTON1_CLICKED != 0):
            if my >= self.y + self.off and my < self.y + self.h - self.off and mx > self.x + self.off and mx < self.x + self.w - self.off - 1:
                # User clicked an item
                self.row_idx_curr = my - self.y - self.off + self.row_idx_view
            elif my == self.y and mx == self.x + self.w - 1 and self.row_idx_curr > 0:
                # User clicked up arrow
                if self.row_idx_curr - 1 < self.row_idx_view:
                    self.row_idx_view -= 1
                self.row_idx_curr -= 1
            elif my == self.y + self.h - 1 and mx == self.x + self.w - 1 and self.row_idx_curr < len(self.options) - 1:
                # User clicked down arrow
                self.row_idx_curr += 1
                if self.row_idx_curr > self.row_idx_view + self.this_h - 1 - self.pad:
                    self.row_idx_view += 1
        if bstate & curses.BUTTON1_DOUBLE_CLICKED != 0:
            if my >= self.y + self.off and my < self.y + self.h - self.off and mx > self.x + self.off and mx < self.x + self.w - self.off:
                # User double-clicked an item. Treat like enter
                self.row_idx_curr = my - self.y - self.off + self.row_idx_view
        self.value = self.options[self.row_idx_curr]
        self.do_event('on_click')


    def change_event(self, idx):
        if idx >= 0 and idx < len(self.options):
            if idx < self.row_idx_view:
                self.row_idx_view = idx
            elif idx > self.row_idx_view + self.h - 1 - self.pad:
                self.row_idx_view = idx - (self.h - 1 - self.pad)
            self.row_idx_curr = idx
            self.value = self.options[self.row_idx_curr]
            self.do_event('on_change')


    def key_event(self, key):
        if key == curses.KEY_UP and self.row_idx_curr > 0:
            self.change_event(self.row_idx_curr - 1)
        elif key == curses.KEY_DOWN and self.row_idx_curr < len(self.options) - 1:
            self.change_event(self.row_idx_curr + 1)
        else:
            # Send key to super for keybinding processing
            super().key_event(key)
        self.do_event('on_keypress')


class group(label):
    def __init__(self, parent, **kwargs):
        self._children = []

        super().__init__(parent=parent, **kwargs)


    def add_child(self, w):
        self._children.append(w)


    def render(self):
#       if self._orig_w == 0: #(0 means scale to fit contents)
#           compute children dims
#           resize based on that
#       else:
#           if 0 < self._orig_w < 1: #(float means proportional to parent)
#               compute parent dims
#               resize based on that
#           else:
#               width is fixed val

#        if self._w_orig == 0:
        w = int(max(self.contents_w + self.pad + 1, self.w))
        h = int(max(self.contents_h + self.pad, self.h))
        cpad = curses.newpad(h, w)
        for idx, widget in enumerate(self._children):
            if not widget.disabled:
                widget.render()
        if self.border:
            self.draw_border(cpad, 0, 0, self.contents_h+self.pad - 1, self.contents_w + self.pad, focus=self._root.widget_focus==self)


    def update_contents(self):
        # Should be good
        # If orig_w,orig_h < 1, x,y,w,h are dependent on children
        # if orig_w==0, then w is only as wide as needed
        w = 0
        h = 0
        for idx, widget in enumerate(self._children):
            w = max(w, widget.x + widget.w)
            h = max(h, widget.y + widget.h)
        self.contents_h = h
        self.contents_w = w
        self.do_event('on_update')


    def resize_event(self):
        # Screen was resized
        self.do_event('on_resize')
        super().resize_event()


    def do_event(self, event):
        for cb,k in self._event_bindings.items():
            if isinstance(k, list) and event in k or event == k:
                cb(self)
                break


    def key_event(self, key):
        # Look for key in widget_manager's key_bindings list
        proc_key = False
        for cb,k in self._key_bindings.items():
            # k can be a list or a number
            if isinstance(k, list) and key in k or key == k:
                # Found key; send self to the specified function for user processing
                self.current_key = key
                cb(self)
                self.current_key = None
                proc_key = True
                break


    def mouse_event(self, mid, mx, my, mz, bstate):
        # Process widgets in reverse, in case newer overwrite older
        for w in reversed(self._children):
            # Hit test
            if my >= w.y and my < w.y + w.h and mx > w.x and mx < w.x + w.w - 1:
                if w in self._widgets_focus:
                    if not w.disabled:
                        # This widget was hit, and can get focus; apply focus and pass mouse event
                        self._focus_ind = self._widgets_focus.index(w)
                        w.mouse_event(mid, mx, my, mz, bstate)
                    break
        self.do_event('on_click')


class widget_manager(group):

    class widgets():
        # Class to hold all widgets. Each widget is stored as object and by name 
        # for easy access during callbacks etc. EG., wm.widgets.label1.value
        # Not used internally by wm
        pass

        # key_bindings={}, event_bindings={}
    def __init__(self, fmt_dialog, quitkey=ord('q'), **kwargs):
        self.name = 'root'
        #self._key_bindings = key_bindings
        #self._event_bindings = event_bindings
        self._quitkey = quitkey
        self._focus_ind = 0       # The index of the widget in widgets_focus with focus
        self._widgets_focus = []  # List of widgets that can receive focus
        self._widgets_all = []    # List of all widgets; used in adding / children etc
        self._mainloop = True     # Set to False to break out of main loop and quit
        self._dialog_min_w = 10
        self._dialog_min_h = 4
        self.x = 0
        self.y = 0
        self.border_types = ('flat', 'line')
        self.keys_enter = [curses.KEY_ENTER, 10, 13]

        # User attributes
        self.wm_event_types = ['on_initialize', 'on_resize', 'on_focus_next', 'on_focus_prev', 'on_got_focus', 'on_lost_focus', 'on_change', 'on_keypress', 'on_click', 'on_exit']
        self.widget_focus = None # The widget that currently has focus
        self.current_key = None  # Holds the keycode that is currently being processed. Helpful in callbacks since multiple keys can call the same func

        self._fmt_dialog_tuple = kwargs.get('fmt_dialog', (-1,-1, curses.A_NORMAL))

        # Initialize curses
        self._win = curses.initscr()

        # Turn off echoing of keys, and enter cbreak mode,
        # where no buffering is performed on keyboard input
        curses.noecho()
        curses.cbreak()

        # Mouse support.
        curses.mousemask(1)

        # Hide cursor
        curses.curs_set(0)
        # In keypad mode, escape sequences for special keys
        # (like the cursor keys) will be interpreted and
        # a special value like curses.KEY_LEFT will be returned
        self._win.keypad(1)
        # Start color, too.  Harmless if the terminal doesn't have
        # color; user can test with has_color() later on.  The try/catch
        # works around a minor bit of over-conscientiousness in the curses
        # module -- the error return from C start_color() is ignorable.
        try:
            curses.start_color()
        except:
            pass

        self.cp_list = [(-1,-1)]
        self._fmt_dialog = curses.color_pair(self.get_color_pair_ind(self._fmt_dialog_tuple[:2])) | self._fmt_dialog_tuple[2]
        super().__init__(root=self, parent=self, name=self.name, value=None, **kwargs)
        self.resize_event()
        self.do_event('on_initialize')


    def dump_screen(self, textfile):
        # Why don't widgets show up? 
        screen_h, screen_w = self._win.getmaxyx()
        with open(textfile, "w") as outfile:
            for row_i in range(screen_h):
                row = ""
                for col_i in range(screen_w):
                    attrs = self._win.inch(row_i, col_i)
                    ch = chr(attrs & 0xFF)
                    row += ch
                outfile.write(row + "\n")


    def dialog_msgbox(self, msg, border=True):

        buttons = ['[ OK ]']
        button_dims = []
        screen_h, screen_w = self._win.getmaxyx()
        content_w = 0
        for l in msg.split("\n"):
            content_w = max(content_w, len(l))
        content_h = msg.count("\n")+1
        if border:
            pad = 2
            off = 1
        else:
            pad = 0
            off = 0

        control_h = 1

        dialog_w = max(content_w + pad, self._dialog_min_w)
        dialog_h = max(content_h + pad + control_h, self._dialog_min_h)

        dialog_x = (screen_w // 2) - (dialog_w // 2)
        dialog_y = (screen_h // 2) - (dialog_h // 2)
        dialog = curses.newwin(dialog_h, dialog_w, dialog_y, dialog_x)
        dialog.bkgd(' ', self._fmt_dialog)
        lines = msg.split("\n")
        for i in range(len(lines)):
            dialog.addstr(off+i, off, lines[i], self._fmt_dialog)
        for button in buttons:
#            if mx >= dialog_x + dialog_w - 7 and mx < dialog_x + dialog_w - 1 and my == dialog_y + dialog_h - 2:
            x = dialog_w - len(button) - off
            y = dialog_h - 1 - off
            abs_x1 = dialog_x + x
            abs_x2 = abs_x1 + len(button)
            abs_y = dialog_y + dialog_h - off - 1 # 1 = button height
            button_dims.append((abs_x1, abs_x2, abs_y))
            dialog.addstr(y, x, button, curses.A_REVERSE)
        dialog.border()
        dialog.refresh()
        while 1:
            key = self._win.getch()
            if key in self.keys_enter:
                break
            elif key == curses.KEY_MOUSE:
                mid, mx, my, mz, bstate = curses.getmouse()
                ret = None
                for button,dims in zip(buttons,button_dims):
                    if mx >= dims[0] and mx < dims[1] and my == dims[2]:
                        ret = button
                        break
                if ret:
                    break

        del dialog
        self._win.touchwin()
        self._win.refresh()
        return ret


    def resize_event(self):
        # x,y are 0 (set in init)
        self.h,self.w = self._win.getmaxyx()


    def get_color_pair_ind(self, cp):

        self.sysdefault_fg,self.sysdefault_bg = curses.pair_content(0)
        if cp[0] == -1:
            fg = self.sysdefault_fg
        else:
            fg = cp[0]
        if cp[1] == -1:
            bg = self.sysdefault_bg
        else:
            bg = cp[1]

        new_cp = (fg,bg)
        if new_cp in self.cp_list:
            this_ind = self.cp_list.index(new_cp)
        else:
            self.cp_list.append(cp) 
            this_ind = len(self.cp_list) - 1
            curses.init_pair(this_ind, fg, bg)
        return this_ind


    def add_widget(self, widget, *args, **kwargs):
        # Parent is a ref to the widget
        # If user specified parent, find it and replace str with actual widget
        if 'parent' in kwargs.keys() and kwargs['parent'] != 'root':
            parent_ind = None
            for this_w in self._widgets_all:
                if this_w.name == kwargs['parent']:
                    parent_ind = self._widgets_all.index(this_w)
                    break
            parent = self._widgets_all[parent_ind]
            kwargs['parent'] = parent
        else:
            # User didn't specify, or specified root
            parent = self
            kwargs['parent'] = self
        # Instantiate widget
        w = widget(root=self, *args, **kwargs)
        # Add widget to the list of all widgets
        self._widgets_all.append(w)
        # Add widget to parent as child
        parent.add_child(w)
        # Add widget to class for easy user access
        setattr(self.widgets, w.name, w)
        if w.get_focus:
            # If it can get focus, add it to the focus list
            #if len(self._widgets_focus) == 0: 
            #    # This is the first widget that can get focus, so
            #    # when main_loop is started, this will be the widget
            #    # with focus
            #    self.widget_focus = widget
            self._widgets_focus.append(w)


    def focus_next(self):
        self._focus_ind += 1
        if self._focus_ind > len(self._widgets_focus) - 1:
            # Cycle back to zero if needed
            self._focus_ind = 0
        if self._widgets_focus[self._focus_ind].disabled:
            # This widget is disabled; move on
            self._focus_ind += 1
            if self._focus_ind > len(self._widgets_focus) - 1:
                # Cycle back to zero if needed
                self._focus_ind = 0
        self.widget_focus = self._widgets_focus[self._focus_ind]
        self.do_event('on_focus_next')


    def focus_prev(self):
        self._focus_ind -= 1
        if self._focus_ind < 0:
            # Cycle back to zero if needed
            self._focus_ind = len(self._widgets_focus) - 1
        if self._widgets_focus[self._focus_ind].disabled:
            # This widget is disabled; move on
            self._focus_ind -= 1
            if self._focus_ind < 0:
                # Cycle back to zero if needed
                self._focus_ind = len(self._widgets_focus) - 1
        self.widget_focus = self._widgets_focus[self._focus_ind]
        self.do_event('on_focus_prev')


    def key_event(self, key):
        # Look for key in widget_manager's key_bindings list
        proc_key = False
        for cb,k in self._key_bindings.items():
            # k can be a list or a number
            if isinstance(k, list) and key in k or key == k:
                # Found key; send self to the specified function for user processing
                self.current_key = key
                cb(self)
                self.current_key = None
                proc_key = True
                break
        if not proc_key:
            # Key not found in key_bindings, send to widget with focus to process
            if len(self._widgets_focus) > 0 and not self._widgets_focus[self._focus_ind].disabled:
                self._widgets_focus[self._focus_ind].key_event(key)
        self.do_event('on_keypress')


    def mouse_event(self, mid, mx, my, mz, bstate):
        # Process widgets in reverse, in case newer overwrite older
        for w in reversed(self._children):
            # Hit test
            if my >= w.y and my < w.y + w.h and mx > w.x and mx < w.x + w.w - 1:
                if w in self._widgets_focus:
                    if not w.disabled:
                        self._win.addstr(2,2,w.name)
                        # This widget was hit, and can get focus; apply focus and pass mouse event
                        self._focus_ind = self._widgets_focus.index(w)
                        w.mouse_event(mid, mx, my, mz, bstate)
                    break
        self.do_event('on_click')


    def quit(self):
        self._mainloop = False
        self.do_event('on_exit')


    def main_loop(self):
        try:
            self.widget_focus = self._widgets_focus[self._focus_ind]
            self.render()
            self._win.refresh()
            self.render()
            self._win.refresh()
            while self._mainloop:
                key = self._win.getch()
                if key == ord('\t'):
                    # Tab key; move focus to the next widget that can get it
                    self.focus_next()
                elif key == curses.KEY_MOUSE:
                    # Mouse event! get data and process
                    mid, mx, my, mz, bstate = curses.getmouse()
                    self._win.addstr(0,0,f"{mx}, {my}")
                    self._win.clrtoeol()
                    self.mouse_event(mid, mx, my, mz, bstate)
                elif key == curses.KEY_RESIZE:
                    # Terminal was resized; call resize_event for all widgets in case they need to re-position
                    self.resize_event()
                elif key == ord('p'):
                    self.dialog_msgbox("This is my first dialog!\nLooks great, lets see how big we can make it\nby adding a third line, and then...\na fourth!")
                    #self.dialog_msgbox("Hey!")
                elif key == self._quitkey:
                    # Quit key hit; break out of loop
                    self.quit()
                else:
                    self.key_event(key)
                self.render()
                self._win.refresh()
                
        finally:
            if self._win:
                self._win.keypad(0)
                curses.mousemask(0)
                curses.echo()
                curses.nocbreak()
                curses.endwin() 


if __name__ == "__main__":

    n = curses.A_NORMAL
    b = curses.A_BOLD
    ret = None
    # Generate list of keycodes to accept (1,2,3,4,5,6,7,8,9,0)
    keys_o = [ord(str(n)) for n in range(10)]
    # Generate map of keycodes -> indices
    # 9 is first because when the user hits 0, that will get converted to index of 9,
    # or the last item in the list. key 0 -> index 9 and likewise, key 1 -> index 0; 
    # key 2 -> index 1 etc 
    keys_i = [9,0,1,2,3,4,5,6,7,8]


    def proc_selection(wm):
        #global ret
        #ret = wm.widget_focus.colors.border.norm
        # We are here because user hit a number key
        # Convert key to an index. Key is stored in current_key
        key = int(chr(wm.current_key))
        # Send index to the change event of the widget with focus
        # We don't know which widget it is, but its the one with focus
        wm.widget_focus.change_event(keys_i[key])
        # Move focus to the next widget
        wm.focus_next()


    def step_focus(wm):
        # Allow the user to use L and R arrows to select widgets
        key = wm.current_key
        if key == curses.KEY_LEFT:
            wm.focus_prev()
        elif key == curses.KEY_RIGHT:
            wm.focus_next()

    def quit(wm):
        # This is the callback for the OK button
        global ret
        ret = "Clicked!"
        wm.dump_screen('test.txt')
        wm.quit()


    selection = {}
    def on_exit(wm):
        global selection
        selection['list1'] = wm.widgets.list1.value
        selection['list2'] = wm.widgets.list2.value
        selection['list3'] = wm.widgets.list3.value
        selection['list4'] = wm.widgets.list4.value
        selection['list5'] = wm.widgets.list5.value

    msg1 = """This is some long text
That spans multiple lines
and has varying width across each line"""

    opt1 = ['1One', '1Two', '1Three', '1Four', '1Five', '1Six', '1Seven', '1Eight', '1Nine', '1Ten']
    opt2 = ['2One', '2Two', '2Three', '2Four', '2Five', '2Six', '2Seven', '2Eight', '2Nine', '2Ten']
    opt3 = ['3One', '3Two', '3Three', '3Four', '3Five', '3Six', '3Seven', '3Eight', '3Nine', '3Ten']
    opt4 = ['4One', '4Two', '4Three', '4Four', '4Five', '4Six', '4Seven', '4Eight', '4Nine', '4Ten']
    opt5 = ['5One', '5Two', '5Three', '5Four', '5Five', '5Six', '5Seven', '5Eight', '5Nine', '5Ten']
    wm = widget_manager(key_bindings={proc_selection: keys_o, step_focus: [curses.KEY_LEFT, curses.KEY_RIGHT]}, event_bindings={on_exit: 'on_exit'}, fmt_dialog=(curses.COLOR_WHITE,curses.COLOR_RED,n)) 
    #wm.add_widget(group, name='group1', x=15, y=0, border='none')
    wm.add_widget(listbox, name='list1', options=opt1, x=14, y=3, w=10, h=8, border='flat', justify='left',
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n), fmt_border_norm=(233,-1,n), fmt_border_focus=(233,-1,n))
    wm.add_widget(listbox, name='list2', options=opt2, x=24, y=3, w=10, h=0, border='flat', justify='left',
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n), fmt_border_norm=(233,-1,n), fmt_border_focus=(233,-1,n))
    wm.add_widget(listbox, name='list3', options=opt3, x=34, y=3, w=10, h=0, border='flat', justify='left',
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n), fmt_border_norm=(233,-1,n), fmt_border_focus=(233,-1,n))
    wm.add_widget(listbox, name='list4', options=opt4, x=44, y=3, w=10, h=0, border='flat', justify='left',
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n), fmt_border_norm=(233,-1,n), fmt_border_focus=(233,-1,n))
    wm.add_widget(listbox, name='list5', options=opt5, x=54, y=3, w=10, h=0, border='flat', justify='left',
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n), fmt_border_norm=(233,-1,n), fmt_border_focus=(233,-1,n))
    wm.add_widget(label, name='label1', value="Choose a word from each list", x=None, y=2, w=0, h=0, border=None, get_focus=False,) 
    wm.add_widget(label, name='label2', value="1\n2\n3\n4\n5\n6\n7\n8\n9\n0", x=13, y=4, w=1, h=0, border=None)
    wm.add_widget(button, name='button1', value="[ OK ]", x=50, y=17, w=0, h=0, border=None, get_focus=True, callback=quit,
        fmt_main_norm=(-1,233,n), fmt_main_focus=(-1,235,n))

    wm.main_loop()

    print(selection)
    print(ret)