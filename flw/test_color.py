import curses


def do_it(win):
    curses.init_pair(1, curses.COLOR_WHITE, 1)
    win.bkgd(' ', curses.color_pair(1))
    win.addstr(1,1, "This is not blue")
    default_fg,default_bg = curses.pair_content(0)
    win.addstr(3,3, f"{default_fg} {default_bg}")
    win.getch()
    win.bkgd(' ', curses.color_pair(1) | curses.A_BOLD | curses.A_REVERSE)
    win.addstr(1,1, "This is now blue")
    win.getch()

if __name__ == '__main__':
    curses.wrapper(do_it)
